# UBBEF
## Extension functions for Saxon

A library containing Integrated Extension functions (http://saxonica.com/documentation/index.html#!extensibility/integratedfunctions) for XSLT and XQUERY transformations using Saxon.

namespace for functions: `http://data.ub.uib.no/ns/saxon/extension-functions#`

### Comparing Text strings
Wrapper for using Simmetrics (https://github.com/Simmetrics/simmetrics)


ubbef:simmetric(s1,s2, alg) Use an empty alg to see legal values for $3 returns xs:float

### Functions for uuid

ubbef:uuid-from-string(s) returns xs:string
Takes string to bytes, and generates a uuid from byte. To create repeatable uuid from unique strings.

ubbef:is-uuid(s) returns xs:boolean


ubbef:random-uuid() return xs:string.


### Tests
Open Oxygen project, select xsl/test/ubbef.xspec.

Run 'Saxon extension function initializer xspec' scenario'.

### To run
add -init:no.uib.saxon.ef.SimmetricsInitializer to command line arguments or write your own init file with a subset or more extension functions. Add lib/* to classpath. (lib only needed for simmetric).

