package no.uib.saxon.ef;

import net.sf.saxon.trans.XPathException;
import org.simmetrics.StringMetric;
import org.simmetrics.metrics.StringMetrics;

import java.util.Arrays;

public enum StringComparatorEnum {


    BLOCK {@Override
 StringMetric similar(){ return StringMetrics.blockDistance(); }},
    COSINE{@Override
StringMetric similar() { return StringMetrics.cosineSimilarity();}},
    DAMERAU{@Override
StringMetric similar() {return StringMetrics.damerauLevenshtein();}},
    DICE {@Override
 StringMetric similar(){ return StringMetrics.dice(); } },
    EUCLIDIAN {@Override
StringMetric similar(){ return StringMetrics.blockDistance();}},
    JACCARD {@Override
 StringMetric similar(){ return StringMetrics.jaccard(); } },
    JARO {@Override
 StringMetric similar(){ return StringMetrics.jaro(); } },
    IDENTITY {@Override
 StringMetric similar(){ return StringMetrics.identity(); } },
    LEVENSHTEIN {@Override
 StringMetric similar(){ return StringMetrics.levenshtein(); }},
    QGRAMS {@Override
 StringMetric similar(){ return StringMetrics.qGramsDistance();} };
    ;


    abstract StringMetric similar();

    public static StringComparatorEnum toEnum(String comparename) throws XPathException {
        StringComparatorEnum comparator;
        try {
            //Fall to default service, if nothing is specified



            comparator = StringComparatorEnum.valueOf(comparename.toUpperCase());
        }
        catch (IllegalArgumentException e){
            String errormsg= "Service parameter is not recognized. Found [" + comparename + "]" +
                    " but expected one of " + Arrays.asList(StringComparatorEnum.values()).toString();

            System.out.print("Service parameter is not recognized. Found [" + comparename + "]" +
                    " but expected one of " + Arrays.asList(StringComparatorEnum.values()).toString());
            /*Fail and do not continue*/
            throw new XPathException(errormsg);
            //throw e;

        }
        return comparator;
    }


}




