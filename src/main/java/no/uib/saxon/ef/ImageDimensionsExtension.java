package no.uib.saxon.ef;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.ma.map.MapType;
import net.sf.saxon.om.GroundedValue;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.s9api.XdmMap;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.Int64Value;
import net.sf.saxon.value.SequenceType;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final public class ImageDimensionsExtension extends ExtensionFunctionDefinition {
    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName("ubbef", "http://data.ub.uib.no/ns/saxon/extension-functions#", "image-dimensions");
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_STRING};
    }

    @Override
    public SequenceType getResultType(SequenceType[] sequenceTypes) {
        return MapType.SINGLE_MAP_ITEM;
    }

    @Override
    public ExtensionFunctionCall makeCallExpression() {
        return new ExtensionFunctionCall() {
            @Override
            public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException {
                String imagePath = arguments[0].head().getStringValue();
                int suffixPos = imagePath.lastIndexOf(".");
                if (suffixPos == -1)
                    throw new XPathException("No file suffix for imagePath: " + imagePath);
                String suffix = imagePath.substring(suffixPos + 1);
                Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
                Path path = Paths.get(imagePath);
                int width;
                int height;
                Map<String, GroundedValue> dimensions = new HashMap<>();

                if (iter.hasNext()) {
                    ImageReader reader = iter.next();
                    try {
                        ImageInputStream stream = new FileImageInputStream(path.toFile());
                        reader.setInput(stream);
                        width = reader.getWidth(reader.getMinIndex());
                        height = reader.getHeight(reader.getMinIndex());
                        dimensions.put("height", new Int64Value(height));
                        dimensions.put("width", new Int64Value(width));
                    } catch (IOException e) {
                        throw new XPathException("Not able to read dimensions of image file: " + imagePath);
                    } finally {
                        reader.dispose();
                    }
                }
                return XdmMap.makeMap(dimensions).getUnderlyingValue();


            }
        };
    }
}