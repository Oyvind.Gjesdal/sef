package no.uib.saxon.ef;

import net.sf.saxon.resource.StandardCollectionFinder;

public class XlsxCollectionFinder
        extends StandardCollectionFinder
{
    @java.lang.Override
    protected boolean isJarFileURI(String collectionURI) {
        return collectionURI.endsWith(".jar") ||  collectionURI.endsWith(".zip") || collectionURI.startsWith("jar:") || collectionURI.endsWith(".xlsx");
    }
}
