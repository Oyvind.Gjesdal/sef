package no.uib.saxon.ef;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.FloatValue;
import net.sf.saxon.value.SequenceType;
import org.simmetrics.StringMetric;


/* function ubbef:simmetric(string1,string2,alg)
WHERE alg is any case string of enum type StringComparatorEnum
returns a float between 0-1 to indicate how similar s1 and s2 are based on alg.
 */
final public class SimmetricsExtension extends ExtensionFunctionDefinition {
    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName("ubbef",    "http://data.ub.uib.no/ns/saxon/extension-functions#", "simmetric");    
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_STRING, SequenceType.SINGLE_STRING,SequenceType.SINGLE_STRING};
    }

    @Override
    public SequenceType getResultType(SequenceType[] sequenceTypes) {
        return SequenceType.SINGLE_FLOAT;
    }

    @Override
    public ExtensionFunctionCall makeCallExpression() {
        return new ExtensionFunctionCall() {
            @Override
            public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException {            
                String s1 =  arguments[0].head().getStringValue();
                String s2 = arguments[1].head().getStringValue();
              
                String alg = arguments[2].head().getStringValue();

                StringMetric metric = StringComparatorEnum.toEnum(alg).similar();

                float result = metric.compare(s1,s2);

                return FloatValue.makeFloatValue(result);
            }
        };
    }
   
}



