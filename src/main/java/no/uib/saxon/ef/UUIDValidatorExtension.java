/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uib.saxon.ef;

import java.util.UUID;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.BooleanValue;
import net.sf.saxon.value.SequenceType;

/**
 *
 * @author Oyvind
 */

final public class UUIDValidatorExtension extends ExtensionFunctionDefinition{

    @Override
    public StructuredQName getFunctionQName() {
                return new StructuredQName("ubbef", "http://data.ub.uib.no/ns/saxon/extension-functions#", "is-uuid");    
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_STRING};
    }

    @Override
    public SequenceType getResultType(SequenceType[] sts) {
        return SequenceType.SINGLE_BOOLEAN;
    }

    @Override
     public ExtensionFunctionCall makeCallExpression() {
        return new ExtensionFunctionCall() {
            @Override
        public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException 
        {  String s = arguments[0].head().getStringValue();
            try { 
                UUID.fromString(s);   
            }            
            catch (IllegalArgumentException ex) { return BooleanValue.FALSE;}        
         return BooleanValue.TRUE;
        }  
      

        };    
    }
}
