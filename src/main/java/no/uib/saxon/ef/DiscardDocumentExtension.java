/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uib.saxon.ef;

import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.DocumentKey;
import net.sf.saxon.om.Item;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.om.TreeInfo;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.trans.XsltController;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.value.SequenceType;

/**
 *
 * @author ogj077
 */
final public class DiscardDocumentExtension  extends ExtensionFunctionDefinition {

    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName("ubbef", "http://data.ub.uib.no/ns/saxon/extension-functions#", "discard-document");    
    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.OPTIONAL_DOCUMENT_NODE};
    }

    @Override
    public SequenceType getResultType(SequenceType[] sts) {
       return SequenceType.OPTIONAL_DOCUMENT_NODE;
    }

    @Override
    public ExtensionFunctionCall makeCallExpression() {
    
     return new ExtensionFunctionCall() {
            @Override
            public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException {            
                try {
                   Item firstItem = arguments[0].head();
                   if (firstItem == null) {
                       return EmptySequence.getInstance();
                   }
                   TreeInfo doc = ((NodeInfo)firstItem).getTreeInfo();              
                   XsltController controller = (XsltController) context.getController();
                   String uri = controller.getDocumentPool().getDocumentURI(doc.getRootNode());
                   if (uri != null) {
                       DocumentKey docUri  = new DocumentKey(uri);
                       controller.removeUnavailableOutputDestination(docUri);
                   }
                   return controller.getDocumentPool().discard(doc).getRootNode();   
                } 
                catch (Exception e) {
                    throw new XPathException(e);
                }
            }
        };
    }
}