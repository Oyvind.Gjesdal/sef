/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.uib.saxon.ef;

import java.io.UnsupportedEncodingException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.SequenceType;
import net.sf.saxon.value.StringValue;

/**
 *
 * @author Oyvind
 * @TODO expand with namespaces as optional param.
 * 6ba7b810-9dad-11d1-80b4-00c04fd430c8 for DNS
 * 6ba7b811-9dad-11d1-80b4-00c04fd430c8 for URL
 * 6ba7b812-9dad-11d1-80b4-00c04fd430c8 for ISO OID
 * 6ba7b814-9dad-11d1-80b4-00c04fd430c8 for X.500 DN
 */
final public class UUIDFromStringExtension extends ExtensionFunctionDefinition  {

    static final String URL_NS ="6ba7b8119dad11d180b400c04fd430c8";

    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName("ubbef", "http://data.ub.uib.no/ns/saxon/extension-functions#", "uuid-from-string");    }

    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_STRING};
    }

    @Override
    public SequenceType getResultType(SequenceType[] sts) {
        return SequenceType.SINGLE_STRING;
    }

    @Override
    public ExtensionFunctionCall makeCallExpression() {
           return new ExtensionFunctionCall() {
            @Override
        public Sequence call(XPathContext context, Sequence[] arguments) throws XPathException  {            
            byte[] uuid = null;    
            try {
                    byte [] ns = DatatypeConverter.parseHexBinary(URL_NS);
                    byte[] name =  arguments[0].head().getStringValue().getBytes("UTF-8");
                    uuid = new byte[ns.length + name.length];
                    System.arraycopy(ns, 0, uuid, 0, ns.length);
                    System.arraycopy(name, 0, uuid, ns.length, name.length);
                    
                    if (name.length==0)
                    {
                        throw new XPathException("function get-uuid-from-string: Parameter needs a a non-empty value");
                    } 
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(UUIDFromStringExtension.class.getName()).log(Level.SEVERE, null, ex);
                }               
        return StringValue.makeStringValue(UUID.nameUUIDFromBytes(uuid).toString());
        }
    };
}
}