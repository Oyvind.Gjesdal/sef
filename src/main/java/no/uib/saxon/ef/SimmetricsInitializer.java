package no.uib.saxon.ef;
import net.sf.saxon.Configuration;

import javax.xml.transform.TransformerException;

public class SimmetricsInitializer implements net.sf.saxon.lib.Initializer {
    @Override
    public void initialize(Configuration configuration) throws TransformerException {      
        configuration.registerExtensionFunction(new SimmetricsExtension());
        configuration.registerExtensionFunction(new UUIDFromStringExtension());
        configuration.registerExtensionFunction(new RandomUUIDExtension());
        configuration.registerExtensionFunction(new UUIDValidatorExtension());
        configuration.registerExtensionFunction(new DiscardDocumentExtension());
        configuration.registerExtensionFunction(new ImageDimensionsExtension());

    }
}
